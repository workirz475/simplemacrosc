/*
 * bit_macros.h
 *
 *  Created on: 12 янв. 2021 г.
 *      Author: avatar
 */

#ifndef SIMPLEMACROSC_BIT_MACROS_H_
#define SIMPLEMACROSC_BIT_MACROS_H_

//Битовые дефайны
#define BIT(bit_no) (1<<(bit_no))
#define BIT_CLEAR(var,bit_no) (var) &= ~(BIT(bit_no))
#define BIT_SET(var,bit_no) ((var)|=(BIT(bit_no)))
#define BIT_GET(var,bit_no) ((var) & (BIT(bit_no)))
#define BIT_SWITCH(var,bit_no) (var) ^= (BIT(bit_no))
#define BIT_BOOL_GET(var,bit_no) (((var) & (BIT(bit_no)))?1:0)
#define BIT_AND_BOOL_GET(var,var1) (((var) & (var1))?1:0)



#endif /* SIMPLEMACROSC_BIT_MACROS_H_ */
